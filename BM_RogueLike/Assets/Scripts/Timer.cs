﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timeLeft = 200;
    public Text countdownText;
    public AudioClip gameOverSound;

    // Use this for initialization
    void Start()
    {
        countdownText = GetComponent<Text>();
    }

    private void CheckIfGameOverT()
    {
        if (timeLeft <= 0)
        {
            SoundController.instance.PlaySingle(gameOverSound);
            SoundController.instance.musicSource.Stop();
            GameController.instance.GameOver();
        }

    }


    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        countdownText.text = "Time Left " + timeLeft.ToString("f0");

        if (timeLeft <= 0)
        {
            CheckIfGameOverT();
            timeLeft = 0f;
        }
    }
}